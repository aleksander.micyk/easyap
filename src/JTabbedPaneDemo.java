import javax.swing.*;
import java.awt.*;

public class JTabbedPaneDemo {
    public JTabbedPaneDemo(){
        //Przygotowanie okna
        JFrame jfrm = new JFrame("JTabbedPaneDemo");
        jfrm.setLayout( new FlowLayout() );
        jfrm.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        jfrm.setSize( 400,200 );

        //Tworzenie panelu
        JTabbedPane jtp = new JTabbedPane();
        jtp.addTab( "Miasto", new CitiesPanel() );
        jtp.addTab( "Kolor", new ColorPanel() );
        jfrm.add( jtp );

        //Wyświetla okno
        jfrm.setVisible( true );

    }
    public static void main(String[] args) {

        //Tworzy okno w wątku obsługi zdarzeń
        SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        new JTabbedPaneDemo();
                    }
                } );
    }
}
