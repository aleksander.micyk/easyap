import javax.swing.*;
import java.awt.*;

public class ColorPanel extends JPanel {
    public ColorPanel(){
        JCheckBox cb1 = new JCheckBox("Red");
        add(cb1);

        JCheckBox cb2 = new JCheckBox("Green");
        add(cb2);
    }
}
